#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 14:07:32 2020

@author: m19001804
"""

import math


class Point:    #classe point
    def __init__(self, x, y):
        self.x=x   #coordonnée x
        self.y=y   #coordonnée y
       
    def x(self):
        "fonction qui retourne la coordonnée x"
        return(self.x)
   
    def y(self):
        "fonction qui retourne la coordonnée y"
        return(self.y)
       
       
    def r(self):
        "fonction qui calcule coordonnée polaire"
        r=math.sqrt(self.x**2+self.y**2) #coordonnée polaire
        return(r)
   
    def t(self):
        "fonction qui calcule coordonnée polaire"
        t=math.atan2(self.y,self.x)
        return(t)

    def __str__(self):
        "affichage str"
        return ('(' + str(self.x) + ',' + str(self.y) + ')')
 
    def __eq__(self, autrePoint):
        "tester egalite"
        return(isinstance(autrePoint, Point) and self.x == autrePoint.x and self.y == autrePoint.y)
       
    def homothetie(self,k):
        "calcul homothétie"
        return(self.x*k, self.y*k)
       
class Polygone :  #classe polygone

    def __init__(self,sommets):
        self.sommets=sommets
       
    def get_sommet(self,i):
        return self.sommets[i]
   
   
    def __str__(self):
        return { ( '[' + str(self.sommets) + ',' +  ']' ) }
   
   
   
    def aire(self):
        "calcul air"
        sum_x=0
        sum_y=0
        for i in range(len(self.sommets)):
            sum_x =self.sommets[i-2].x+self.sommets[i-1].x
            sum_y =self.sommets[i-2].y-self.sommets[i-1].y
           
        sum_x=abs(sum_x)
        sum_y=abs(sum_y)
        return(sum_y*sum_x*0.5)

             

class Triangle(Polygone):  #classe triangle qui hérite de polygone
    def __init__(self, a, b, c):
        l=[a,b,c]
       
        super().__init__(l)
       
    def __str__(self):   #affichage str
        return (super().__str__())
       
   
class Rectangle(Polygone): #classe Rectagle qui hérite de polygone
    def __init__(self, xMin, xMax, yMin, yMax):
        l=[xMin,yMin, xMax, yMax]
        super().__init__(l)
       
    def __str__(self):
        return(super().__str__())
   
class PolygoneRegulier(Polygone):  #classe polygone régulier qui hérite aussi de polygone
    def __init__(self,liste_points,centre, rayon , nombreSommets):
        super().__init__(liste_points)
        self.centre=centre
        self.rayon=rayon
        self.nombreSommets=nombreSommets